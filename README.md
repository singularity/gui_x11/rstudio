# Singularity build image with X11 application
### use case Rstudio

#### Rstudio V 1.1.456 with R_base Version: 3.6.1


a) build image
```bash
singularity build Rstudio_v3.6.sif Singularity.Rstudio_v3.6.def
```

 - When we have finish to install & ready to use R inside the container.
```bash
# and execute Rtudio inside the container
./Rstudio_v3.6.sif
```

